@echo off
:build
call gradlew clean build
echo Control+C to close, any other key to rebuild.
pause
goto build
